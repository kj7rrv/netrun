#!/usr/bin/env python3
import platform
import textwrap
import os

language = f"Python {platform.python_version()}"
operating_system = f"{platform.system()} {platform.release()}"
machine = f"a system with a(n) {platform.processor()} processor"

message = f"You are running {language} on {operating_system} on {machine}."

for line in textwrap.wrap(message, os.get_terminal_size().columns):
    print(line)
