# netrun

Run scripts from the Internet in a way that is safer and more secure than
running `curl <url> | bash`

## Work in Progress

`netrun` is still experimental; don't rely on its working properly.

## Try it

    git clone https://git.kj7rrv.com/kj7rrv/netrun
    cd netrun
    python3 -m pip install requests # See note
    python3 netrun.py https://git.kj7rrv.com/kj7rrv/netrun/raw/branch/master/demos/shell.sh
    python3 netrun.py --interpreter=python3 https://git.kj7rrv.com/kj7rrv/netrun/raw/branch/master/demos/python.py
    python3 netrun.py --help
    python3 -m pip install requests # Or

Note: You could also install requests with your distro's package manager; just
don't use pip as root or with sudo.

## Installation

### Single user

`requests` must be installed either with `pip` (run as the same user that is
installing and will use `netrun`), or with your distro's package manager; do
not run `pip` as root or with `sudo`

    cp netrun.py ~/.local/bin/netrun
    chmod +x ~/.local/bin/netrun
    netrun https://git.kj7rrv.com/kj7rrv/netrun/raw/branch/master/demos/shell.sh

### System-wide

Install `requests` using your distro's package manager first; using `pip` as a
regular user only installs it for that user, and using it as root/with `sudo`
is not recommended

    sudo cp netrun.py /usr/bin/netrun
    sudo chmod +x /usr/bin/netrun 
    sudo netrun https://git.kj7rrv.com/kj7rrv/netrun/raw/branch/master/demos/shell.sh
